#!/bin/sh

BIN=node

start() {
  set -e

  echo ======== Starting e2e tests ========

  $BIN main.js
}

case $1 in
  start) "$@"; exit;;
esac
