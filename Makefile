ENV_FILE ?= .env
CONTAINER_NAME ?= colmena_e2e
IMAGE_NAME ?= colmena_e2e
NETWORK_NAME ?= local_colmena_devops
DOCKERFILE_DIR ?= devops/builder/
COMPOSE_DIR ?= devops/local/

GREEN=\033[0;32m
YELLOW=\033[0;33m
NOFORMAT=\033[0m

# Add env variables if needed
ifneq (,$(wildcard ${ENV_FILE}))
	include ${ENV_FILE}
    export
endif

default: help

.PHONY: help
#❓ help: @ Displays this message
help: SHELL := /bin/sh
help:
	@echo ""
	@echo "List of available MAKE targets for development usage."
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@echo "Examples:"
	@echo ""
	@echo "	make ${GREEN}start${NOFORMAT}		- Run the integration test"
	@echo ""
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(firstword $(MAKEFILE_LIST))| tr -d '#'  | awk 'BEGIN {FS = ":.*?@ "}; {printf "${GREEN}%-30s${NOFORMAT} %s\n", $$1, $$2}'
	@echo ""

#🐳 devops.build: @ Builds a new image for the e2e service.
docker.build:
	@docker build ./ -f $(DOCKERFILE_DIR)/Dockerfile -t $(IMAGE_NAME)

#🐳 docker.connect: @ Connect to the e2e running container
docker.connect:
	@docker exec -it $(CONTAINER_NAME) /bin/bash

#🐳 docker.delete: @ Delete the e2e docker container
docker.delete: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.delete:
	@docker rm $(CONTAINER_NAME) 2> /dev/null || true

#🐳 docker.e2e.start: @ Starts the compose service
docker.e2e.start:
	@cd ${COMPOSE_DIR} && docker compose up

#🐳 docker.e2e.stop: @ Stops and removes containers, networks
docker.e2e.stop:
	@cd ${COMPOSE_DIR} && docker compose stop

#🐳 docker.e2e.down: @ Starts the compose service
docker.e2e.down:
	@cd ${COMPOSE_DIR} && docker compose down

#🐳 docker.logs: @ Show logs for the docker container
docker.logs: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.logs:
	@docker logs $(CONTAINER_NAME)

#🐳 docker.logs.watch: @ Watch logs for the docker container
docker.logs.watch: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.logs.watch:
	@docker logs $(CONTAINER_NAME) -f

#🐳 docker.release: @ Re-create a docker image and run it
docker.release: docker.stop docker.delete docker.build docker.run

#🐳 docker.rerun: @ Stops and deletes old container to re-run a fresh new container
docker.rerun: docker.stop docker.delete docker.run

#🐳 docker.run: @ Run the e2e docker instance
docker.run: PORT:=5555
docker.run: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.run: IMAGE_NAME:=$(IMAGE_NAME)
docker.run:
	@docker run \
		--cap-add=SYS_ADMIN \
		--privileged \
		--detach \
		--name $(CONTAINER_NAME) \
		--network $(NETWORK_NAME) \
		-p 5555:80 \
		--env PORT=5555 \
		--env-file .env.prod $(IMAGE_NAME)

#🐳 docker.stop: @ Stop the e2e docker container
docker.stop: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.stop:
	@docker container stop $(CONTAINER_NAME) 2> /dev/null || true

#💻 lint: @ Runs eslint fixes
.PHONY: lint
lint:
	@npm run lint:fix

#📦 setup: @ Install requirements 
setup:
	@npm install

#🧪 start.watch: @ Start and watch the e2e tests 
start.watch:
	@node watch.js

#🧪 start: @ Start the e2e tests 
.PHONY: start
start:
	@npm start
