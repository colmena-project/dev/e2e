import nodemon from 'nodemon'
import signale from 'signale'

nodemon({ script: './main.js' })
  .on('start', function () {
    signale.info("Watching the main process...")
  })
  .on('crash', function () {
    signale.error("Script crashed!")
  })
  .on('restart', function () {
    signale.info("Watcher restarting...")
  })
