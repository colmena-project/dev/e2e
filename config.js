const config = {
  TYPING_DELAY: process.env.TYPING_DELAY,
  backendUrl: process.env.BASE_BACKEND_URL,
  emailHost: process.env.EMAIL_HOST,
  emailHostScheme: process.env.EMAIL_HOST_SCHEME,
  frontendUrl: process.env.BASE_FRONTEND_URL,
  headless: process.env.HEADLESS === "true" ? true : false,
  superadminEmail: process.env.SUPERADMIN_EMAIL,
  superadminPassword: process.env.SUPERADMIN_PASSWORD
}

export default config

console.info("Reading environment configuration:\n")
console.table(config)
