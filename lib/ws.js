import WebSocket from 'ws'
import signale from 'signale'

const socketScope = signale.scope("WebSocket")

export const webSocketFactory = {
  connect: function(url) {
    return new Promise(resolve => {
      const ws = new WebSocket(url);
      ws.addEventListener("error", error => {
        socketScope.error(`Received error=${error}`)
        // readyState === 3 is CLOSED
        if (error.target.readyState === 3) {
          this.connectionTries--;
  
          if (this.connectionTries > 0) {
            setTimeout(() => this.connect(url), 5000);
          } else {
            throw new Error(
              "Maximum number of connection trials has been reached"
            );
          }
        }
      });

      ws.addEventListener("close", () => {
        socketScope.debug("Socket connection closed")
      })
  
      resolve(ws)
    })
  },
  connectionTries: 3
}

export function waitForMessage(ws, signaleScope, callback) {
  return new Promise(resolve => {
    ws.on("message", buffer => {
      const payload = JSON.parse(buffer.toString())
      const messageId = payload.id
      signaleScope.debug(
        `Received email with id ${messageId}`
      )
      resolve(messageId)
    })
    callback()
  })
}

export async function waitForMessages(ws, signaleScope, callback, messagesQty) {
  return new Promise((resolve, reject) => {
    const messages = []
    ws.on("message", buffer => {
      const payload = JSON.parse(buffer.toString())
      const messageId = payload.id
      signaleScope.debug(
        `Received email with id ${messageId}`
      )
      messages.push(payload)
      if (messages.length === messagesQty){
        resolve(messages)
      }
    })

    setTimeout(() => {
      reject(new Error('Request timed out'))
    }, 5000);

    callback()
  })
}

export function waitForConnection(ws, signaleScope) {
  return new Promise(resolve => {
    ws.on("open", () => {
      signaleScope.debug("Socket connection opened")
      resolve()
    })
  })
}
