import Signale from 'signale'

const signaleOptions = {
  types: {
    complete: {
      badge: '✅',
      color: 'green',
      label: 'complete'
    },
    debug: {
      badge: '🔍',
      color: 'cyan',
      label: 'debug',
      logLevel: 'debug'
    },
    info: {
      badge: '💬',
      color: 'white',
      label: 'info',
      logLevel: 'info'
    }
  }
}

export const signale = new Signale.Signale(signaleOptions)
