export const userLinkSelectors = {
  logoutLink: '#logout-button'
}

export const adminLoginSelectors = {
  passwordInput: '#password-input',
  submitButton: 'button[class="btn waves-effect waves-light"]',
  usernameInput: '#username-input'
}

export const staffSuperadminInvitationSelectors = {
  africaRegionOption: "//span[contains(., 'Africa')]",
  emailInput: '#id_email',
  fullnameInput: '#id_full_name',
  groupsSelect: 'input[class="select-dropdown dropdown-trigger"]',
  // eslint-disable-next-line max-len
  languagesSelect: '//*[@id="userinvitation_form"]/div/div/fieldset[2]/div[3]/div/div/input',
  // eslint-disable-next-line max-len
  regionsSelect: '//*[@id="userinvitation_form"]/div/div/fieldset[2]/div[4]/div/div/input',
  spanishLanguageOption: "//span[contains(., 'Español')]",
  staffGroupOption: "//span[contains(., 'Staff')]",
  submitActionsRow: '.submit-row',
  submitButton: 'button[class="default waves-effect waves-light btn"]',
  usernameInput: '#id_username'
}

export const staffSuperadminInvitationConfirmationSelectors = {
  emailInput: '#id_email',
  englishLanguageOption: "//span[contains(., 'English')]",
  fullnameInput: '#id_full_name',
  // eslint-disable-next-line max-len
  languagesSelect: '//*[@id="content"]/form/div[5]/div/input',
  passwordConfirmationInput: '#id_new_password_confirmation',
  passwordInput: '#id_new_password',
  submitButton: '//*[@id="content"]/form/div[11]/input',
  usernameInput: '#id_username'
}

export const newOrganizationSelectors = {
  adminEmailInput: '#id_admin_email',
  adminFullnameInput: '#id_admin_full_name',
  adminUsernameInput: '#id_admin_username',
  // eslint-disable-next-line max-len
  countriesSelect: '//*[@id="organization_form"]/div/div/fieldset/div[2]/div/div/input',
  countrySelectOption: "//span[contains(., 'Uganda')]",
  englishLanguageOption: "//span[contains(., 'English')]",
  // eslint-disable-next-line max-len
  languagesSelect: '//*[@id="organization_form"]/div/div/fieldset/div[7]/div/div/input',
  organizationNameInput: '#id_name',
  submitButton: '//*[@id="organization_form"]/div/div/div[2]/div[2]/button'
}

export const pwaCreateAccountSelectors = {
  acceptTermsAndConditionsButton: '#accept_terms_and_conditions_button',
  createAccountButton: '#sign_in_up_submit_button',
  passwordInput: '#password_text_input',
  signInButton: '#sign_in_submit_button',
  termsOfUseCheckbox: '#terms_and_conditions_check_box',
  termsOfUseCloseButton: '#terms_and_conditions_close_button',
  usernameInput: '#username_text_input'
}

export const pwaLogInSelectors = {
  addInvitationButton: '#open_invitation_view_button',
  adminRoleSelectOption: '#selector_4',
  andorraSelectOption: '#selector_AD',
  collaboratorRoleSelectOption: '#selector_5',
  continueToThirdStepButton: '#handle_save_profile_submit_button',
  deleteInvitationButton: '#delete_0_button',
  emailInput: '#username_text_input',
  emailInvitationInput: '#email_invitation_text_input',
  fullnameInput: '#username_profile_name_text_input',
  fullnameInvitationInput: '#username_invitation_text_input',
  goToHomeButton: '#go_to_home_button',
  hamburguerMenuButton: '#nav-toggle',
  invitationConfirmationButton: '#accept_invitation_data_button',
  invitationSubmitButton: '#add_invitation_button',
  logoutButton: '#logout_button',
  mediaLocationSelect: '#organization_country_select',
  mediaNameInput: '#organization_profile_name_text_input',
  passwordInput: '#password_text_input',
  preferedLanguegeSelect: '#organization_language_select',
  publicContactEmailInput: '#organization_profile_email_text_input',
  roleSelect: '#role_invitation_select',
  saveMyProfileSubmitButton: '#save_my_profile_submit_button',
  sendInvitationsSubmitButton: '#send_invitations_submit_button',
  signInButton: '#sign_in_up_submit_button',
  spanishSelectOption: '#selector_1',
  startConfigurationButton: '#start_configuration_click_button',
  successfulModal: '#pf-modal-part-3',
  updateEmailInput: '#username_email_text_input'
}

export const pwaCollaboratorLogInSelector = {
  createAccountButton: '#sign_in_up_submit_button',
  passwordInput: '#password_text_input',
  signInButton: '#sign_in_up_submit_button',
  termsAndConditionsCheckBox: '#terms_and_conditions_check_box',
  termsAndConditionsCloseButton: '#terms_and_conditions_close_button',
  userNameInput: '#username_text_input'
}
