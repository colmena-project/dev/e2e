import signale from 'signale'

const apiScope = signale.scope("Mailcrab API")

export async function getMessage(url, id) {
  return fetch(`${url}/message/${id}`)
  .then(response => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    return response.json(); // Parse response body as JSON
  })
  .then(data => {
    // Handle the JSON data
    apiScope.debug(`Successfully fetched message with id=${data.id}.`)
    return data
  })
  .catch(error => {
    apiScope.error(
      // eslint-disable-next-line max-len
      `An error occurred while fetching the message with id=${id} error=${error}`
    )
    throw new Error(error);
  });
}
