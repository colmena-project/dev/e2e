import { pwaLogInSelectors } from "./selectors.js"

async function logOut(page, signale) {
  // Open the side bar
  const sideBarHamburguerMenuButton = await page.waitForSelector(
    pwaLogInSelectors.hamburguerMenuButton
  )

  await sideBarHamburguerMenuButton.click()

  signale.debug('Open the side bar')

  // Click the logout button
  const logoutPWAButton = await page.waitForSelector(
    pwaLogInSelectors.logoutButton
  )

  await logoutPWAButton.click()

  signale.debug('Log out')
}

export default {
  logOut
}
