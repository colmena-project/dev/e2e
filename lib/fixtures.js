import { UniqueEnforcer } from 'enforce-unique';
import { faker } from '@faker-js/faker';

export const createUser = () => {
  const uniqueEnforcer = new UniqueEnforcer();
  const firstName = faker.person.firstName()
  const lastName = faker.person.firstName()
  const password = faker.internet.password({length: 12, prefix: 'abA1._'})

  return {
    email: uniqueEnforcer.enforce(() => {
      return faker.internet.email({firstName, lastName})
    }),
    fullName: `${firstName} ${lastName}`,
    password,
    passwordConfirmation: password,
    username: uniqueEnforcer.enforce(() => {
      return faker.internet.userName({firstName, lastName})
    })
  }
}

export const createOrganization = () => {
  const uniqueEnforcer = new UniqueEnforcer()
  const domain = faker.helpers.arrayElement(['Coop', 'ORG', 'ONG'])
  const fantasyName = faker.person.firstName()
  const otherFantasyName = faker.person.firstName()
  const name = uniqueEnforcer.enforce(() => {
    return `${fantasyName} ${otherFantasyName} ${domain}`
  })

  return {
    name
  }
}
