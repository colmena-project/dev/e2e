const ANCHOR_REGEX = /<a href="(https?:\/\/[^ ]*)">/g;

export function getUrlFromStaffInvitation(inputString, signaleScope) {
  const regex = /(https?:\/\/.*\/invitations\/[^ ]*\/)/g;
  return _getUrlFromEmail(inputString, regex, signaleScope)
}

export function getUrlFromStaffConfirmation(inputString, signaleScope) {
  return _getUrlFromEmail(inputString, ANCHOR_REGEX, signaleScope)
}

export function getUrlFromAccountConfirmation(inputString, signaleScope) {
  return _getUrlFromEmail(inputString, ANCHOR_REGEX, signaleScope)
}

export function getUrlFromOrganizationInvitation(inputString, signaleScope) {
  const regex = /(https?:\/\/.*\/auth\/signup\/[^ ]*\/)/g;
  return _getUrlFromEmail(inputString, regex, signaleScope)
}

export function _getUrlFromEmail(inputString, regex, signaleScope) {
  const matches = [...inputString.matchAll(regex)]

  if (matches.length > 0 && matches[0][1]) {
    const [extractedUrl] = matches[0][1].trim().split('"');
    signaleScope.debug(`Found invitation link in message url=${extractedUrl}`)
    return extractedUrl
  } else {
    const errorMsg = 'No URL found'
    signaleScope.error(errorMsg)
    throw Error(errorMsg)
  }
}

export function getUrlFromEmail(inputString, signaleScope) {
  const regex = /(https?:\/\/[^ ]*)/g;
  const matches = [...inputString.matchAll(regex)]

  if (matches.length > 0 && matches[0][1]) {
    const [extractedUrl] = matches[0][1].trim().split('"');
    signaleScope.debug(`Found invitation link in message url=${extractedUrl}`)
    return extractedUrl
  } else {
    const errorMsg = 'No URL found'
    signaleScope.error(errorMsg)
    throw Error(errorMsg)
  }
}
