import {
  adminLoginSelectors,
  newOrganizationSelectors,
  pwaCollaboratorLogInSelector,
  pwaCreateAccountSelectors,
  pwaLogInSelectors,
  staffSuperadminInvitationConfirmationSelectors,
  staffSuperadminInvitationSelectors,
  userLinkSelectors
} from './lib/selectors.js'
import { createOrganization, createUser } from './lib/fixtures.js'
import {
  getUrlFromAccountConfirmation,
  getUrlFromOrganizationInvitation,
  getUrlFromStaffConfirmation,
  getUrlFromStaffInvitation
} from './lib/utils.js'
import {
  waitForConnection,
  waitForMessage,
  waitForMessages,
  webSocketFactory
} from './lib/ws.js'
import commands from './lib/commands.js'
import config from './config.js'
import { getMessage } from './lib/api.js'
import puppeteer from 'puppeteer';
import { signale } from './lib/signale.js'

const sleep = ms => new Promise(r => setTimeout(r, ms));

// eslint-disable-next-line no-unexpected-multiline
(async () => {
  console.log("");
  console.log(".--''- COLMENA E2E TESTS --'--.");
  console.log("      .-.         .--''-.");
  console.log("      .-.         .--''-.");
  console.log("    .'   '.     /'       `.");
  console.log("    '.     '. ,'          |");
  console.log(" o    '.o   ,'        _.-'");
  console.log("  \\.--./'. /.:. :._:.'");
  console.log(" .\\   /'._-':#0: ':#0: ':");
  console.log(":(#) (#) :  ':#0: ':#0: ':>#=--");
  console.log(" ' ____ .'_.:J0:' :J0:' :'");
  console.log("  'V  V'/ | |\":' :'\":'");
  console.log("        \\  \\ \\");
  console.log("        '  ' '");
  console.log("");

  const { TYPING_DELAY } = config

  const baseAdminUrl = config.backendUrl
  const superadminEmail = config.superadminEmail
  const superadminPassword = config.superadminPassword
  const emailHostScheme = config.emailHostScheme
  const emailApiUrl = `${emailHostScheme}://${config.emailHost}/api`
  const emailListenerUrl = `ws://${config.emailHost}/ws`

  // Launch the browser and open a new blank page
  const browser = await puppeteer.launch({
    args: [`--window-size=1227,760`],
    headless: config.headless
  });
  const superadminPage = await browser.newPage();

  try {
    // Set screen size
    await superadminPage.setViewport({height: 760, width: 1227});

    const defaultSignaleScope = signale.scope('E2E Status')
    defaultSignaleScope.start('Starting use cases simulation')

    /**
     * Superadmin login
     */

    const aSuperadminUserInvitesAStaffUser = signale
      .scope('1. A Superadmin user invites a Staff user')
    aSuperadminUserInvitesAStaffUser.addSecrets([superadminPassword])
    aSuperadminUserInvitesAStaffUser
      .time('A superadmin user will invite a staff user')
    aSuperadminUserInvitesAStaffUser
      .info('Login as a superadmin')

    // Navigate the page to a URL
    await superadminPage.goto(baseAdminUrl);

    const usernameInput = adminLoginSelectors.usernameInput
    await superadminPage.waitForSelector(usernameInput)
    await superadminPage.type(
      usernameInput,
      superadminEmail,
      {delay: TYPING_DELAY}
    )
    aSuperadminUserInvitesAStaffUser.debug(
      `Input the superadmin email ${superadminEmail}`
    )

    const passwordInput = adminLoginSelectors.passwordInput
    await superadminPage.waitForSelector(passwordInput)
    await superadminPage.type(
      passwordInput,
      superadminPassword,
      {delay: TYPING_DELAY}
    )
    aSuperadminUserInvitesAStaffUser.debug(
      `Input the superadmin password ${superadminPassword}`
    )

    // Wait and click on first result
    const loginSubmitButtonSelector = adminLoginSelectors.submitButton
    await superadminPage.waitForSelector(loginSubmitButtonSelector)
    await superadminPage.click(loginSubmitButtonSelector);
    aSuperadminUserInvitesAStaffUser.debug('Click the submit button')

    await sleep(2000)

    /**
     * Invite a Staff User
     */

    const staffUser = createUser()

    await superadminPage.goto(
      `${baseAdminUrl}/en/admin/accounts/userinvitation/add/`
    )
    aSuperadminUserInvitesAStaffUser.debug('Visit the user invitation section')

    const groupSelector = staffSuperadminInvitationSelectors.groupsSelect
    await superadminPage.waitForSelector(groupSelector)
    await superadminPage.click(groupSelector)
    const [staffGroupOption] = await superadminPage
      .$x(staffSuperadminInvitationSelectors.staffGroupOption);
    await staffGroupOption.click()
    aSuperadminUserInvitesAStaffUser.debug('Select the staff group')
    await sleep(250)

    // Type username
    const usernameSelector = staffSuperadminInvitationSelectors.usernameInput
    await superadminPage.waitForSelector(usernameSelector)
    await superadminPage.type(
      usernameSelector,
      staffUser.username,
      {delay: TYPING_DELAY}
    )
    aSuperadminUserInvitesAStaffUser.debug(
      `Type a username '${staffUser.username}'`
    )
    await sleep(250)

    // Type full name
    const fullNameSelector = staffSuperadminInvitationSelectors.fullnameInput
    await superadminPage.waitForSelector(fullNameSelector)
    await superadminPage.type(
      fullNameSelector, staffUser.fullName, {delay: TYPING_DELAY}
    )
    aSuperadminUserInvitesAStaffUser.debug(
      `Type a full name '${staffUser.fullName}'`
    )
    await sleep(250)

    // Type email
    const emailSelector = staffSuperadminInvitationSelectors.emailInput
    await superadminPage.waitForSelector(emailSelector)
    await superadminPage.type(
      emailSelector,
      staffUser.email,
      {delay: TYPING_DELAY}
    )
    aSuperadminUserInvitesAStaffUser.debug(`Type an email '${staffUser.email}'`)
    await sleep(250)

    // Choose language

    const [languageSelector] = await superadminPage.$x(
      staffSuperadminInvitationSelectors.languagesSelect
    )
    await languageSelector.click()
    const [spanishLanguageOption] = await superadminPage.$x(
      staffSuperadminInvitationSelectors.spanishLanguageOption
    );
    await spanishLanguageOption.click()
    aSuperadminUserInvitesAStaffUser.debug('Choose the spanish language')
    await superadminPage.keyboard.press('Escape');
    await sleep(250)

    // Choose region
    const [regionSelector] = await superadminPage.$x(
      staffSuperadminInvitationSelectors.regionsSelect
    )

    await regionSelector.click()
    const [africaLanguageOption] = await superadminPage.$x(
      staffSuperadminInvitationSelectors.africaRegionOption
    );
    await africaLanguageOption.click()
    aSuperadminUserInvitesAStaffUser.debug('Choose the Africa region')
    await superadminPage.keyboard.press('Escape');
    await sleep(250)

    const staffInvitationListener = await webSocketFactory.connect(
      emailListenerUrl
    );

    await waitForConnection(
      staffInvitationListener, aSuperadminUserInvitesAStaffUser
    )

    // Submit form
    const submitRowSelector =
      staffSuperadminInvitationSelectors.submitActionsRow
    const submitRowElement = await superadminPage.waitForSelector(
      submitRowSelector
    )
    const submitButton = await submitRowElement.waitForSelector(
      staffSuperadminInvitationSelectors.submitButton
    )

    const buttonClickCallback = async () => {
      await submitButton.click()
      aSuperadminUserInvitesAStaffUser.debug('Click the submit button')
    }

    const aStaffUserCompletesTheUserInvitation = signale.scope(
      "2. A Staff completes the user invitation"
    )

    const staffInvitationEmailMessageId = await waitForMessage(
      staffInvitationListener,
      aStaffUserCompletesTheUserInvitation,
      buttonClickCallback
    )

    aStaffUserCompletesTheUserInvitation.info(
      'A Staff user receives an invitation to manage Colmena regions'
    )

    await staffInvitationListener.close()

    aSuperadminUserInvitesAStaffUser.complete(
      `An email has been sent to ${staffUser.email}`
    )
    aSuperadminUserInvitesAStaffUser.timeEnd()

    // Log out the superadmin
    aSuperadminUserInvitesAStaffUser.debug(
      `Logging out superadmin ${superadminEmail}...`
    )
    const logoutButton = await superadminPage.waitForSelector(
      userLinkSelectors.logoutLink
    )
    await logoutButton.click()
    aSuperadminUserInvitesAStaffUser.debug(
      `Logged out superadmin ${superadminEmail}...`
    )

    await superadminPage.close()

    await sleep(250)

    /**
     * A Staff user completes the invitation
     */

    const updatedStaffUser = createUser()

    aStaffUserCompletesTheUserInvitation.addSecrets([
      updatedStaffUser.password,
      updatedStaffUser.passwordConfirmation
    ])

    aStaffUserCompletesTheUserInvitation.time(
      "A Staff user will complete the registation setup"
    )

    const staffInvitationEmailMessage = await getMessage(
      emailApiUrl, staffInvitationEmailMessageId
    )

    const staffUserConfirmationUrl = getUrlFromStaffInvitation(
      staffInvitationEmailMessage.html,
      aStaffUserCompletesTheUserInvitation
    )

    const staffUserConfirmationPage = await browser.newPage()
    await staffUserConfirmationPage.goto(staffUserConfirmationUrl)

    // TODO: input invalid fields before submitting a successful form.

    // Edit staff fullname
    const staffUserConfirmationFullnameSelector =
      staffSuperadminInvitationConfirmationSelectors.fullnameInput
    await staffUserConfirmationPage.waitForSelector(
      staffUserConfirmationFullnameSelector
    )
    await staffUserConfirmationPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      staffUserConfirmationFullnameSelector, { clickCount: 3 }
    )
    await staffUserConfirmationPage.type(
      staffUserConfirmationFullnameSelector,
      updatedStaffUser.fullName,
      {delay: TYPING_DELAY}
    )
    aStaffUserCompletesTheUserInvitation.debug(
      `Type an updated fullname '${updatedStaffUser.fullName}'`
    )

    // Edit staff username
    const staffUserConfirmationUsernameSelector =
      staffSuperadminInvitationConfirmationSelectors.usernameInput
    await staffUserConfirmationPage.waitForSelector(
      staffUserConfirmationUsernameSelector
    )
    await staffUserConfirmationPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      staffUserConfirmationUsernameSelector, { clickCount: 3 }
    )
    await staffUserConfirmationPage.type(
      staffUserConfirmationUsernameSelector,
      updatedStaffUser.username,
      {delay: TYPING_DELAY}
    )
    aStaffUserCompletesTheUserInvitation.debug(
      `Type an updated username '${updatedStaffUser.username}'`
    )

    // Edit staff email
    const staffUserConfirmationEmailSelector =
      staffSuperadminInvitationConfirmationSelectors.emailInput
    await staffUserConfirmationPage.waitForSelector(
      staffUserConfirmationEmailSelector
    )
    await staffUserConfirmationPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      staffUserConfirmationEmailSelector, { clickCount: 3 }
    )
    await staffUserConfirmationPage.type(
      staffUserConfirmationEmailSelector,
      updatedStaffUser.email,
      {delay: TYPING_DELAY}
    )
    aStaffUserCompletesTheUserInvitation.debug(
      `Type an updated email '${updatedStaffUser.email}'`
    )

    // Edit language
    const [staffConfirmationLanguageSelector] =
      await staffUserConfirmationPage.$x(
        staffSuperadminInvitationConfirmationSelectors.languagesSelect
      )
    await staffConfirmationLanguageSelector.click()
    const [staffConfirmationEnglishLanguageOption] = 
      await staffUserConfirmationPage.$x(
        staffSuperadminInvitationConfirmationSelectors.englishLanguageOption
      );
    await staffConfirmationEnglishLanguageOption.click()
    aStaffUserCompletesTheUserInvitation.debug('Choose the english language')

    // Click outside the languages select
    await staffUserConfirmationPage.click(staffUserConfirmationEmailSelector)

    // Input new password
    const staffUserConfirmationPasswordSelector =
      staffSuperadminInvitationConfirmationSelectors.passwordInput
    await staffUserConfirmationPage.waitForSelector(
      staffUserConfirmationPasswordSelector
    )
    await staffUserConfirmationPage.type(
      staffUserConfirmationPasswordSelector,
      updatedStaffUser.password,
      {delay: TYPING_DELAY}
    )
    aStaffUserCompletesTheUserInvitation.debug(
      `Type an updated password '${updatedStaffUser.password}'`
    )

    // Confirm new password
    const staffUserConfirmationPasswordConfirmationSelector =
      staffSuperadminInvitationConfirmationSelectors.passwordConfirmationInput
    await staffUserConfirmationPage.waitForSelector(
      staffUserConfirmationPasswordConfirmationSelector
    )
    await staffUserConfirmationPage.type(
      staffUserConfirmationPasswordConfirmationSelector,
      updatedStaffUser.passwordConfirmation,
      {delay: TYPING_DELAY}
    )
    aStaffUserCompletesTheUserInvitation.debug(
      // eslint-disable-next-line max-len
      `Type an updated password confirmation '${updatedStaffUser.passwordConfirmation}'`
    )

    // TODO: assert the account type is Admin

    // TODO: assert the region is the previously selected (Africa, in this case)

    const staffUserConfirmationListener = await webSocketFactory.connect(
      emailListenerUrl
    );

    await waitForConnection(
      staffUserConfirmationListener, aStaffUserCompletesTheUserInvitation
    )

    // Submit
    await sleep(250)
    const [staffSuperadminConfirmationSubmitButton] =
      await staffUserConfirmationPage.$x(
        staffSuperadminInvitationConfirmationSelectors.submitButton
      )
    
    const staffConfirmationButtonClickCallback = async () => {
      await staffSuperadminConfirmationSubmitButton.click()
      aStaffUserCompletesTheUserInvitation.debug('Click the submit button')
    }

    const staffInvitationConfirmationEmailMessageId = await waitForMessage(
      staffUserConfirmationListener,
      aStaffUserCompletesTheUserInvitation,
      staffConfirmationButtonClickCallback
    )

    await staffUserConfirmationListener.close()

    aStaffUserCompletesTheUserInvitation.complete(
      `An email has been sent to ${staffUser.email}`
    )
    aStaffUserCompletesTheUserInvitation.timeEnd()

    await staffUserConfirmationPage.close()

    /**
     * A Staff user creates an organization
     */

    const aStaffUserCreatesAnOrganization = signale.scope(
      "3. A Staff user creates an organization"
    )

    aStaffUserCreatesAnOrganization.info(
      'Staff user invites an Admin to a new Organization'
    )

    aStaffUserCreatesAnOrganization.addSecrets([
      updatedStaffUser.password,
      updatedStaffUser.passwordConfirmation
    ])

    aStaffUserCreatesAnOrganization.time(
      // eslint-disable-next-line max-len
      "A Staff user will create an organization and invite a user to administrate it"
    )

    const staffConfirmationEmailMessage = await getMessage(
      emailApiUrl, staffInvitationConfirmationEmailMessageId
    )

    const staffConfirmationLoginUrl = getUrlFromStaffConfirmation(
      staffConfirmationEmailMessage.html,
      aStaffUserCreatesAnOrganization
    )

    const staffUserPage = await browser.newPage();
    await staffUserPage.goto(staffConfirmationLoginUrl)

    await staffUserPage.waitForSelector(usernameInput)
    await staffUserPage.type(
      usernameInput,
      updatedStaffUser.email,
      {delay: TYPING_DELAY}
    )
    aStaffUserCreatesAnOrganization.debug(
      `Input the staff email ${updatedStaffUser.email}`
    )

    await staffUserPage.waitForSelector(passwordInput)
    await staffUserPage.type(
      passwordInput,
      updatedStaffUser.password,
      {delay: TYPING_DELAY}
    )
    aStaffUserCreatesAnOrganization.debug(
      `Input the staff password ${updatedStaffUser.password}`
    )

    // Wait and click on first result
    await staffUserPage.waitForSelector(loginSubmitButtonSelector)
    await staffUserPage.click(loginSubmitButtonSelector);
    aStaffUserCreatesAnOrganization.debug('Click the submit button')

    await sleep(2000)

    // Visit the add organization page
    await staffUserPage.goto(
      `${baseAdminUrl}/en/admin/organizations/organization/add/`
    )

    aStaffUserCreatesAnOrganization.debug('Visit the add organization section')
    
    const newOrganization = createOrganization()
    const newOrganizationAdminUser = createUser()

    // Type an organization name
    const newOrganizationNameSelector =
      newOrganizationSelectors.organizationNameInput
    await staffUserPage.waitForSelector(newOrganizationNameSelector)
    await staffUserPage.type(
      newOrganizationNameSelector,
      newOrganization.name,
      {delay: TYPING_DELAY}
    )
    aStaffUserCreatesAnOrganization.debug(
      `Type an organization name '${newOrganization.name}'`
    )

    // Select an organization country
    const [newOrganizationCountrySelector] =
    await staffUserPage.$x(newOrganizationSelectors.countriesSelect)
    await newOrganizationCountrySelector.click()
    const [newOrganizationUgandaCountryOption] = 
      await staffUserPage.$x(newOrganizationSelectors.countrySelectOption);
    await newOrganizationUgandaCountryOption.click()
    aStaffUserCreatesAnOrganization.debug('Select the Uganda country')

    // Type an Admin user email
    const newOrganizationAdminEmailInput =
      newOrganizationSelectors.adminEmailInput
    await staffUserPage.waitForSelector(newOrganizationAdminEmailInput)
    await staffUserPage.type(
      newOrganizationAdminEmailInput,
      newOrganizationAdminUser.email,
      {delay: TYPING_DELAY}
    )
    aStaffUserCreatesAnOrganization.debug(
      `Type an admin user email '${newOrganizationAdminUser.email}'`
    )

    // Type an Admin full name
    const newOrganizationAdminFullnameInput =
      newOrganizationSelectors.adminFullnameInput
    await staffUserPage.waitForSelector(newOrganizationAdminFullnameInput)
    await staffUserPage.type(
      newOrganizationAdminFullnameInput,
      newOrganizationAdminUser.fullName,
      {delay: TYPING_DELAY}
    )
    aStaffUserCreatesAnOrganization.debug(
      `Type an admin user fullname '${newOrganizationAdminUser.fullName}'`
    )

    // Type an Admin username
    const newOrganizationAdminUsernameInput =
      newOrganizationSelectors.adminUsernameInput
    await staffUserPage.waitForSelector(newOrganizationAdminUsernameInput)
    await staffUserPage.type(
      newOrganizationAdminUsernameInput,
      newOrganizationAdminUser.username,
      {delay: TYPING_DELAY}
    )
    aStaffUserCreatesAnOrganization.debug(
      `Type an admin user username '${newOrganizationAdminUser.username}'`
    )

    // Select an organization language
    const [newOrganizationLanguageSelector] =
    await staffUserPage.$x(newOrganizationSelectors.languagesSelect)
    await newOrganizationLanguageSelector.click()
    const [newOrganizationEnglishLanguageOption] = 
      await staffUserPage.$x(newOrganizationSelectors.englishLanguageOption);
    await newOrganizationEnglishLanguageOption.click()
    aStaffUserCreatesAnOrganization.debug('Select the english language')
    await sleep(250)

    const [newOrganizationSubmitButton] =
      await staffUserPage.$x(newOrganizationSelectors.submitButton)
    
    const newOrganizationButtonClickCallback = async () => {
      await newOrganizationSubmitButton.click()
      aStaffUserCreatesAnOrganization.debug('Click the submit button')
    }

    const newOrganizationListener = await webSocketFactory.connect(
      emailListenerUrl
    );

    await waitForConnection(
      newOrganizationListener, aStaffUserCreatesAnOrganization
    )

    const newOrganizationInvitationEmailMessageId = await waitForMessage(
      newOrganizationListener,
      aStaffUserCreatesAnOrganization,
      newOrganizationButtonClickCallback
    )

    await newOrganizationListener.close()

    aStaffUserCreatesAnOrganization.complete(
      `An email has been sent to ${newOrganizationAdminUser.email}`
    )
    aStaffUserCreatesAnOrganization.timeEnd()

    await staffUserPage.close()

    /**
     * An Admin user confirms the account
     */

    const anAdminLogsInForFirstTimeScope =
    "4. An Admin user logs in for the first time"

    const anAdminUserLogsInForTheFirstTime = signale.scope(
      anAdminLogsInForFirstTimeScope
    )
    
    anAdminUserLogsInForTheFirstTime.time(
      "An Admin user will logs in for first time in the pwa"
    )

    const newAdminUserConfirmationEmailMessage = await getMessage(
      emailApiUrl, newOrganizationInvitationEmailMessageId
    )
    
    const newOrganizationAdminConfirmationUrl =
    getUrlFromOrganizationInvitation(
      newAdminUserConfirmationEmailMessage.html,
      anAdminUserLogsInForTheFirstTime
    )

    anAdminUserLogsInForTheFirstTime.info(
      'An Admin user received an invitation to manage an organization'
    )

    const organizationAdminPage = await browser.newPage();
    await organizationAdminPage.goto(newOrganizationAdminConfirmationUrl)

    anAdminUserLogsInForTheFirstTime.debug(
      `Visit ${newOrganizationAdminConfirmationUrl}`
    )

    // TODO: test texts with different languages

    const updateOrganizationAdminUser = newOrganizationAdminUser
    anAdminUserLogsInForTheFirstTime.addSecrets([
      updateOrganizationAdminUser.password,
      updateOrganizationAdminUser.passwordConfirmation
    ])

    // Accept Terms and Conditions
    const acceptTermsAndConditionsButton = pwaCreateAccountSelectors
    .acceptTermsAndConditionsButton
    await organizationAdminPage.waitForSelector(
      acceptTermsAndConditionsButton
    )
    await organizationAdminPage.click(acceptTermsAndConditionsButton)

    // Type a new Admin username
    const createAccountUsernameInput = pwaCreateAccountSelectors.usernameInput
    await organizationAdminPage.waitForSelector(createAccountUsernameInput)
    await organizationAdminPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      createAccountUsernameInput, { clickCount: 3 }
    )
    await organizationAdminPage.type(
      createAccountUsernameInput,
      updateOrganizationAdminUser.username,
      {delay: TYPING_DELAY}
    )
    anAdminUserLogsInForTheFirstTime.debug(
      `Type an admin username '${updateOrganizationAdminUser.username}'`
    )
    
    // Type a password
    const createAccountPasswordInput = pwaCreateAccountSelectors.passwordInput
    await organizationAdminPage.waitForSelector(createAccountPasswordInput)
    await organizationAdminPage.type(
      createAccountPasswordInput,
      updateOrganizationAdminUser.password,
      {delay: TYPING_DELAY}
    )
    anAdminUserLogsInForTheFirstTime.debug(
      `Type an admin password '${updateOrganizationAdminUser.password}'`
    )
    
    // Submit the form: press create account button
    const createAccountButton =
    await organizationAdminPage.waitForSelector(
      pwaCreateAccountSelectors.createAccountButton
    )
    const createAccountButtonClickCallback = async () => {
      await createAccountButton.click()
      anAdminUserLogsInForTheFirstTime.debug(
        'Click the create account button'
      )
    }

    // connect a listener to catch emails
    const createAccountListener = await webSocketFactory.connect(
      emailListenerUrl
    );
    await waitForConnection(
      createAccountListener, anAdminUserLogsInForTheFirstTime
    )

    // get the create account email
    const completeCreateAccountEmailMessageId = await waitForMessage(
      createAccountListener,
      anAdminUserLogsInForTheFirstTime,
      createAccountButtonClickCallback
    )

    // close the listener
    createAccountListener.close()

    // read the email message
    const completeCreateAccountEmailMessage = await getMessage(
      emailApiUrl, completeCreateAccountEmailMessageId
    )

    // take the sign in url from the email
    const signInUrl = getUrlFromAccountConfirmation(
      completeCreateAccountEmailMessage.html,
      anAdminUserLogsInForTheFirstTime
    )

    /**
    * As an Admin user I want to login for the first time
    * and configure my profile.
    */

    // go to sign in page
    const adminSignInPage = await browser.newPage();
    await adminSignInPage.goto(signInUrl)

    // Type a new Admin email
    const signInEmailInput = pwaLogInSelectors.emailInput
    await adminSignInPage.waitForSelector(signInEmailInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      signInEmailInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      signInEmailInput, updateOrganizationAdminUser.email, {
        clickCount: 3, delay: TYPING_DELAY
      }
    )
    anAdminUserLogsInForTheFirstTime.debug(
      `Type an admin email '${updateOrganizationAdminUser.email}'`
    )

    // Type a password
    const signInPasswordInput = pwaLogInSelectors.passwordInput
    await adminSignInPage.waitForSelector(signInPasswordInput)
    await adminSignInPage.type(
      signInPasswordInput,
      updateOrganizationAdminUser.password,
      { clickCount: 3, delay: TYPING_DELAY }
    )
    anAdminUserLogsInForTheFirstTime.debug(
      `Type an admin password '${updateOrganizationAdminUser.password}'`
    )

    // click the sign in button
    const signInButton =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.signInButton
      )
    await signInButton.click()
    anAdminUserLogsInForTheFirstTime.debug('Click the sign in button')

    // click the start configuration button
    const startConfigurationButton = await adminSignInPage.waitForSelector(
      pwaLogInSelectors.startConfigurationButton
    )
    startConfigurationButton.click()
    anAdminUserLogsInForTheFirstTime.debug(
      'Click the start configuration button'
    )

    /* First Step */

    const adminUserUserProfileWizardStep = signale.scope(
      anAdminLogsInForFirstTimeScope,
      "1. Wizard: profile update"
    )

    adminUserUserProfileWizardStep.info(
      'An Admin user updates the profile'
    )

    // Creates a new user to update the admin user profile
    const updateOrganizationAdminUserPWA = createUser()

    // Type a new Admin fullname
    const logInFullnameInput = pwaLogInSelectors.fullnameInput
    await adminSignInPage.waitForSelector(logInFullnameInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      logInFullnameInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      logInFullnameInput,
      updateOrganizationAdminUserPWA.fullName,
      {delay: TYPING_DELAY}
    )
    adminUserUserProfileWizardStep.debug(
      // eslint-disable-next-line max-len
      `Type an updated organization admin fullname '${updateOrganizationAdminUserPWA.fullName}'`
    )

    // Type a new Admin email
    const logInEmailInput = pwaLogInSelectors.updateEmailInput
    await adminSignInPage.waitForSelector(logInEmailInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      logInEmailInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      logInEmailInput,
      updateOrganizationAdminUserPWA.email,
      {delay: TYPING_DELAY}
    )
    adminUserUserProfileWizardStep.debug(
      // eslint-disable-next-line max-len
      `Type an updated organization admin email '${updateOrganizationAdminUserPWA.email}'`
    )

    // click the sign in button
    const saveMyProfileSubmitButton =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.saveMyProfileSubmitButton
      )
    await saveMyProfileSubmitButton.click()
    adminUserUserProfileWizardStep.debug('Click the Save My Profile button')

    /* Second step */

    const adminUserOrganizationDetailsWizardStep = signale.scope(
      anAdminLogsInForFirstTimeScope,
      "2. Wizard: organization details"
    )

    adminUserOrganizationDetailsWizardStep.info(
      'An Admin user updates the organization details'
    )

    const updateOrganizationAttrs = createOrganization()
    const updateOrganizationAdminAttrs = createUser()

    // Type an organization name
    const mediaNameInput = pwaLogInSelectors.mediaNameInput
    await adminSignInPage.waitForSelector(mediaNameInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      mediaNameInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      mediaNameInput,
      updateOrganizationAttrs.name,
      {delay: TYPING_DELAY}
    )
    adminUserOrganizationDetailsWizardStep.debug(
      `Type an organization name '${updateOrganizationAttrs.name}'`
    )

    // Type an organization public contact email
    const publicContactInput = pwaLogInSelectors.publicContactEmailInput
    await adminSignInPage.waitForSelector(publicContactInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      publicContactInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      publicContactInput,
      updateOrganizationAdminAttrs.email,
      {delay: TYPING_DELAY}
    )
    adminUserOrganizationDetailsWizardStep.debug(
      `Type an organization public contact email '${
        updateOrganizationAdminAttrs.email
      }'`
    )

    // Edit organization location
    const organizationLocationSelector =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.mediaLocationSelect
      )
    await organizationLocationSelector.click()
    const organizationAndorraLocationSelectOption =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.andorraSelectOption
      );
    await organizationAndorraLocationSelectOption.click()
    adminUserOrganizationDetailsWizardStep.debug('Choose the Andorra region')

    // Click outside the locations select
    await adminSignInPage.click(mediaNameInput)

    // Edit organization language
    const organizationLanguageSelector =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.preferedLanguegeSelect
      )
    await organizationLanguageSelector.click()
    const organizationSpanidhLanguageSelectOption =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.spanishSelectOption
      );
    await organizationSpanidhLanguageSelectOption.click()
    adminUserOrganizationDetailsWizardStep.debug('Choose the spanish language')

    // Click outside the locations select
    await adminSignInPage.click(mediaNameInput)

    // click the continue button
    const continueToThirdStepButton = await adminSignInPage.waitForSelector(
      pwaLogInSelectors.continueToThirdStepButton
    )
    continueToThirdStepButton.click()

    adminUserOrganizationDetailsWizardStep.debug(
      'Click the continue to third step button'
    )

    /* Third Step */

    const adminUserUserInvitationsWizardStep = signale.scope(
      anAdminLogsInForFirstTimeScope,
      "3. Wizard: user invitations"
    )

    adminUserUserInvitationsWizardStep.info(
      'An Admin user invites new members to the organization'
    )

    // Adds an Admin role user
    const adminInvitation = createUser()

    // Type the admin invitation fullname
    const adminFullnameInvitationInput = pwaLogInSelectors
    .fullnameInvitationInput

    await adminSignInPage.waitForSelector(adminFullnameInvitationInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      adminFullnameInvitationInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      adminFullnameInvitationInput,
      adminInvitation.fullName,
      {delay: TYPING_DELAY}
    )
    adminUserUserInvitationsWizardStep.debug(
      `Type an organization public contact username '${
        adminInvitation.fullName
      }'`
    )

    // Type the admin invitation email
    const adminEmailInvitationInput = pwaLogInSelectors.emailInvitationInput
    await adminSignInPage.waitForSelector(adminEmailInvitationInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      adminEmailInvitationInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      adminEmailInvitationInput,
      adminInvitation.email,
      {delay: TYPING_DELAY}
    )
    adminUserUserInvitationsWizardStep.debug(
      `Type an organization public contact email '${
        adminInvitation.email
      }'`
    )

    // Edit admin invitation role
    const adminRoleSelector =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.roleSelect
      )
    await adminRoleSelector.click()
    const adminRoleSelectOption =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.adminRoleSelectOption
      );
    await adminRoleSelectOption.click()
    adminUserUserInvitationsWizardStep.debug('Choose the admin user role')

    // click the continue button
    const adminInvitationSubmitButton = await adminSignInPage.waitForSelector(
      pwaLogInSelectors.invitationSubmitButton
    )
    adminInvitationSubmitButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the invitation submit button'
    )

    // click the confirmation button
    const adminInvitationConfirmationButton = await adminSignInPage
    .waitForSelector(
      pwaLogInSelectors.invitationConfirmationButton
    )

    adminInvitationConfirmationButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the invitation confirmation submit button'
    )

    // click the add more members button
    const collaboratorAddMemberButton = await adminSignInPage.waitForSelector(
      pwaLogInSelectors.addInvitationButton
    )
    collaboratorAddMemberButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the add more members button'
    )

    // Adds an collaborator role user
    const collaboratorInvitation = createUser()

    // Type the admin invitation fullname
    const collaboratorFullnameInvitationInput = pwaLogInSelectors
    .fullnameInvitationInput

    await adminSignInPage.waitForSelector(collaboratorFullnameInvitationInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      collaboratorFullnameInvitationInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      collaboratorFullnameInvitationInput,
      collaboratorInvitation.fullName,
      {delay: TYPING_DELAY}
    )
    adminUserUserInvitationsWizardStep.debug(
      `Type an organization public contact username '${
        collaboratorInvitation.fullName
      }'`
    )

    // Type the admin invitation email
    const collaboratorEmailInvitationInput = pwaLogInSelectors
    .emailInvitationInput

    await adminSignInPage.waitForSelector(collaboratorEmailInvitationInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      collaboratorEmailInvitationInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      collaboratorEmailInvitationInput,
      collaboratorInvitation.email,
      {delay: TYPING_DELAY}
    )
    adminUserUserInvitationsWizardStep.debug(
      `Type an organization public contact email '${
        collaboratorInvitation.email
      }'`
    )

    // Edit admin invitation role
    const collaboratorRoleSelector =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.roleSelect
      )
    await collaboratorRoleSelector.click()
    const collaboratorRoleSelectOption =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.collaboratorRoleSelectOption
      );
    await collaboratorRoleSelectOption.click()
    adminUserUserInvitationsWizardStep.debug('Choose collaborator user role')

    // click the continue button
    const collaboratorInvitationSubmitButton = await adminSignInPage
    .waitForSelector(
      pwaLogInSelectors.invitationSubmitButton
    )

    collaboratorInvitationSubmitButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the invitation submit button'
    )

    // click the confirmation button
    const collaboratorInvitationConfirmationButton = await adminSignInPage
    .waitForSelector(
      pwaLogInSelectors.invitationConfirmationButton
    )
    collaboratorInvitationConfirmationButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the invitation confirmation submit button'
    )

    // Delete the admin user
    // Await for the invitations list
    await sleep(2000)

    // click the delete button
    const deleteInvitationButton = await adminSignInPage.waitForSelector(
      pwaLogInSelectors.deleteInvitationButton
    )

    deleteInvitationButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Delete first invitation'
    )

    // click the add more members button
    const adminAddMemberButton = await adminSignInPage.waitForSelector(
      pwaLogInSelectors.addInvitationButton
    )
    adminAddMemberButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the add more members button'
    )

    // Type the admin invitation fullname
    const sameAdminFullnameInvitationInput = pwaLogInSelectors
    .fullnameInvitationInput

    await adminSignInPage.waitForSelector(adminFullnameInvitationInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      sameAdminFullnameInvitationInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      sameAdminFullnameInvitationInput,
      adminInvitation.fullName,
      {delay: TYPING_DELAY}
    )
    adminUserUserInvitationsWizardStep.debug(
      `Type an organization public contact username '${
        adminInvitation.fullName
      }'`
    )

    // Type the admin invitation email
    const sameAdminEmailInvitationInput = pwaLogInSelectors.emailInvitationInput
    await adminSignInPage.waitForSelector(adminEmailInvitationInput)
    await adminSignInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      sameAdminEmailInvitationInput, { clickCount: 3 }
    )
    await adminSignInPage.type(
      sameAdminEmailInvitationInput,
      adminInvitation.email,
      {delay: TYPING_DELAY}
    )
    adminUserUserInvitationsWizardStep.debug(
      `Type an organization public contact email '${
        adminInvitation.email
      }'`
    )

    // Edit admin invitation role
    const twiceAdminRoleSelector =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.roleSelect
      )
    await twiceAdminRoleSelector.click()
    const twiceAdminRoleSelectOption =
      await adminSignInPage.waitForSelector(
        pwaLogInSelectors.adminRoleSelectOption
      );
    await twiceAdminRoleSelectOption.click()
    adminUserUserInvitationsWizardStep.debug('Choose the admin user role')

    // click the continue button
    const twiceAdminInvitationSubmitButton = await adminSignInPage
    .waitForSelector(
      pwaLogInSelectors.invitationSubmitButton
    )

    await twiceAdminInvitationSubmitButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the invitation submit button'
    )

    // click the confirmation button
    const twiceAdminInvitationConfirmationButton = await adminSignInPage
    .waitForSelector(
      pwaLogInSelectors.invitationConfirmationButton
    )

    await twiceAdminInvitationConfirmationButton.click()

    adminUserUserInvitationsWizardStep.debug(
      'Click the invitation confirmation submit button'
    )

    // click the send invitation submit button
    const sendInvitationsListSubmitButton = await adminSignInPage
    .waitForSelector(
      pwaLogInSelectors.sendInvitationsSubmitButton
    )

    // open the listener
    const invitationsListener = await webSocketFactory.connect(
      emailListenerUrl
    );

    // callback to click the submit button
    const sendInvitationsButtonClickCallback = async () => {
      await sendInvitationsListSubmitButton.click()
      adminUserUserInvitationsWizardStep.debug(
        'Click the invitations submit button'
      )
    }

    // get the invitation emails
    const invitationEmailMessages = await waitForMessages(
      invitationsListener,
      adminUserUserInvitationsWizardStep,
      sendInvitationsButtonClickCallback,
      2
    )

    // close the listener
    invitationsListener.close()

    // admin user receives an invitation email
    const adminMessageId = invitationEmailMessages.find(
      msg => msg.envelope_recipients.includes(
        adminInvitation.email
      )
    ).id

    adminUserUserInvitationsWizardStep.debug(
      `Admin user ${
        adminInvitation.fullName
      } received invitation email with id ${
        adminMessageId
      }`
    )

    // collaborator user receives an invitation email
    const collaboratorMessageId = invitationEmailMessages.find(
      msg => msg.envelope_recipients.includes(
        collaboratorInvitation.email
      )
    ).id

    adminUserUserInvitationsWizardStep.debug(
      `Collaborator user ${
        collaboratorInvitation.fullName
      } received invitation email with id ${
        collaboratorMessageId
      }`
    )

    // checks the successful invitation modal exists
    await adminSignInPage.waitForSelector(
      pwaLogInSelectors.successfulModal
    )

    adminUserUserInvitationsWizardStep.debug(
      'Open the invitation sent successful modal'
    )

    // click the modal Go Home button
    const modalGoHomeButton = await adminSignInPage.waitForSelector(
      pwaLogInSelectors.goToHomeButton
    )

    await modalGoHomeButton.click()

    adminUserUserInvitationsWizardStep.debug('Click the go home modal button')

    await commands.logOut(adminSignInPage, adminUserUserInvitationsWizardStep)

    adminUserUserInvitationsWizardStep.timeEnd()

    // a Collaborator user logs in for the first time
    const aCollaboratorUserLogsInForTheFirstTime = signale.scope(
      "5. A Collaborator user logs in for the first time"
    )

    const collaboratorUser = collaboratorInvitation

    aCollaboratorUserLogsInForTheFirstTime.time(
      "Logging in for first time in the pwa"
    )

    // get the link for the collaborator first log in
    const collaboratorInvitationEmailMessage = await getMessage(
      emailApiUrl, collaboratorMessageId
    )

    const collaboratorUserConfirmationUrl =
      await getUrlFromOrganizationInvitation(
        collaboratorInvitationEmailMessage.html,
        aCollaboratorUserLogsInForTheFirstTime
      )

    aCollaboratorUserLogsInForTheFirstTime.info(
      'An invited collaborator received an invitation to join the organization'
    )

    // go to the log registration page
    const collaboratorLogInPage = await browser.newPage();

    await collaboratorLogInPage.goto(collaboratorUserConfirmationUrl)

    // Accept Terms and Conditions
    const collaboratorAcceptTermsAndConditionsButton = pwaCreateAccountSelectors
    .acceptTermsAndConditionsButton
    await collaboratorLogInPage.waitForSelector(
      collaboratorAcceptTermsAndConditionsButton
    )
    await collaboratorLogInPage.click(
      collaboratorAcceptTermsAndConditionsButton
    )

    // Type the collaborator user email
    const collaboratorEmailInput = pwaCollaboratorLogInSelector
    .userNameInput

    await collaboratorLogInPage.waitForSelector(collaboratorEmailInput)
    await collaboratorLogInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      collaboratorEmailInput, { clickCount: 3 }
    )
    await collaboratorLogInPage.type(
      collaboratorEmailInput,
      collaboratorUser.email,
      {delay: TYPING_DELAY}
    )
    aCollaboratorUserLogsInForTheFirstTime.debug(
      `Type an organization public contact email '${
        collaboratorUser.email
      }'`
    )

    // Type the collaborator password
    const collaboratorPasswordInput = pwaCollaboratorLogInSelector.passwordInput

    await collaboratorLogInPage.waitForSelector(collaboratorPasswordInput)
    await collaboratorLogInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      collaboratorPasswordInput, { clickCount: 3 }
    )
    await collaboratorLogInPage.type(
      collaboratorPasswordInput,
      collaboratorUser.password,
      {delay: TYPING_DELAY}
    )
    aCollaboratorUserLogsInForTheFirstTime.debug(
      `Type an organization public contact password '${
        collaboratorUser.password
      }'`
    )

    // Click the Create my account button
    const collaboratorCreateMyAccountButton = await collaboratorLogInPage
    .waitForSelector(
      pwaCollaboratorLogInSelector.createAccountButton
    )

    // Press create account button again
    const createAccountButtonCallback = async () => {
      await collaboratorCreateMyAccountButton.click()
      aCollaboratorUserLogsInForTheFirstTime.debug(
        'Click the create my account button'
      )
    }

    // connect a listener to catch emails
    const createCollaboratorAccountListener = await webSocketFactory.connect(
      emailListenerUrl
    );
    await waitForConnection(
      createCollaboratorAccountListener, aCollaboratorUserLogsInForTheFirstTime
    )

    // get the create account email
    const collaboratorCreateAccountEmailMessageId = await waitForMessage(
      createCollaboratorAccountListener,
      aCollaboratorUserLogsInForTheFirstTime,
      createAccountButtonCallback
    )

    aCollaboratorUserLogsInForTheFirstTime.info(
      'Another invited collaborator received an invitation' +
      'from a former collaborator to join the organization'
    )

    // close the listener
    createCollaboratorAccountListener.close()

    // read the email message
    const collaboratorCreateAccountEmailMessage = await getMessage(
      emailApiUrl, collaboratorCreateAccountEmailMessageId
    )

    // take the sign in url from the email
    const collaboratorSignInUrl = getUrlFromAccountConfirmation(
      collaboratorCreateAccountEmailMessage.html,
      aCollaboratorUserLogsInForTheFirstTime
    )

    // Go to Sign In page
    await collaboratorLogInPage.goto(collaboratorSignInUrl)

    // Type the collaborator user email
    const collaboratorSignInEmailInput = pwaCollaboratorLogInSelector
    .userNameInput

    await collaboratorLogInPage.waitForSelector(collaboratorSignInEmailInput)
    await collaboratorLogInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      collaboratorSignInEmailInput, { clickCount: 3 }
    )
    await collaboratorLogInPage.type(
      collaboratorSignInEmailInput,
      collaboratorUser.email,
      {delay: TYPING_DELAY}
    )
    aCollaboratorUserLogsInForTheFirstTime.debug(
      `Type an organization public contact email '${
        collaboratorUser.email
      }'`
    )

    // Type the collaborator password
    const collaboratorSignInPasswordInput = pwaCollaboratorLogInSelector
    .passwordInput

    await collaboratorLogInPage.waitForSelector(collaboratorSignInPasswordInput)
    await collaboratorLogInPage.click(
      // Click 3 times so that the text is selected and then cleared when typed.
      collaboratorSignInPasswordInput, { clickCount: 3 }
    )
    await collaboratorLogInPage.type(
      collaboratorSignInPasswordInput,
      collaboratorUser.password,
      {delay: TYPING_DELAY}
    )
    aCollaboratorUserLogsInForTheFirstTime.debug(
      `Type an organization public contact password '${
        collaboratorUser.password
      }'`
    )

    // Click the Sign In button
    const collaboratorSignInButton = await collaboratorLogInPage
    .waitForSelector(
      pwaCollaboratorLogInSelector.signInButton
    )

    await collaboratorSignInButton.click()

    aCollaboratorUserLogsInForTheFirstTime.debug(
      'Click the Sign In button'
    )

    await commands.logOut(
      collaboratorLogInPage,
      aCollaboratorUserLogsInForTheFirstTime
    )

    aCollaboratorUserLogsInForTheFirstTime.timeEnd()

    // defaultSignaleScope.timeEnd()
    defaultSignaleScope.success("Tests finished successfully!")

  } catch (error) {
    console.error(error)
  } finally {
    await browser.close();
  }
})();
