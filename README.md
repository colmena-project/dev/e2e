# Colmena E2E

<p align="center" style="margin-top: 14px;">
  <a href="https://gitlab.com/colmena-project/dev/e2e/-/pipelines?page=1&scope=all&ref=dev">
    <img
      src="https://gitlab.com/colmena-project/dev/e2e/badges/dev/pipeline.svg?key_text=dev%20pipeline&key_width=80"
      alt="Dev Pipeline Status"
    >
  </a>
  <a href="https://gitlab.com/colmena-project/dev/e2e/-/pipelines?page=1&scope=all&ref=main">
    <img
      src="https://gitlab.com/colmena-project/dev/e2e/badges/main/pipeline.svg?key_text=staging%20pipeline&key_width=96"
      alt="Staging Pipeline Status"
    >
  </a>
  <a href="https://gitlab.com/colmena-project/dev/e2e/-/blob/dev/LICENSE">
    <img
      src="https://img.shields.io/badge/License-GPL%20v3-white.svg"
      alt="License"
    >
  </a>
</p>

`e2e` is a [Pupeteer](https://pptr.dev/) application to help us run end to end tests across the whole platform.

## Requirements

The e2e needs the node language to be installed. You can install it manually or via the `asdf` package manager, using the fixed version from the *.tool-versions* files

* Ubuntu/Debian SO
* Nodejs: `18.17.1` or greater
* asdf: (optional) [installation instructions](https://asdf-vm.com/guide/getting-started.html#_2-download-asdf)
* Docker [`24.0.2`](https://docs.docker.com/desktop/install/ubuntu/): used to run dockerized images of the e2e.
* Compose plugin [`v2.3.3`](https://docs.docker.com/compose/install/linux/): used to run the whole plaftform simulation.

### SO requirements

This project does not currently depend on any library to be installed in the operating system.

### Dependencies

The e2e can be run using the local environment. This includes: a potsgres database, the backend instance and the frontend instance. Go back to the [colmena-devops repository](https://gitlab.com/colmena-project/dev/colmena-devops/-/blob/main/README.md) and set them up to continue. Then get a [colmena server](https://gitlab.com/colmena-project/dev/bcakend/-/blob/main/README.md) and the [colmena client](https://gitlab.com/colmena-project/dev/frontend/-/blob/main/README.md) started locally.

The e2e test can also be executed using the dockerized version to get a more production-like environment.

Finally, the docker compose approach does not depend on any external applications, the whole environment is replicated in a single docker compose file.

## Getting started

### Local Environment

Make a copy of the base env file.

```bash
cp .env.example .env
```

* `BASE_BACKEND_URL`: The backend host. Default: `http://localhost:8000`
* `BASE_FRONTEND_URL`: The frontend host. Default: `http://localhost:5173`
* `EMAIL_HOST`: The mailing service hostname. Default: `localhost:1080`
* `EMAIL_HOST_SCHEME`: The mailing service host scheme. Default: `http`
* `SUPERADMIN_EMAIL`: The superadmin email in a backend instance. Default: `admin@email`
* `SUPERADMIN_PASSWORD`: The superadmin password in a backend instance. Default: `somepassword`
* `HEADLESS`: Whether the browser should run in headless mode. Default: `true`
* `TYPING_DELAY`: Assign typing speed on input interactions. Default: `15`

We use `make` to quickly run predefined tasks to setup, start and stop our environment.

```bash
# Type make to display the default help message.
# You'll notice a bunch of suggestions that you can use to manage the database, skip them for now, they'll be useful as
# a reminder.
make
```

Install node requirements

```bash
make setup
```

Start the e2e test

```bash
make start
```

### Dockerized environment

> Note: Remember that if the backend or the frontend instances are running in a dockerized environment you'll probably have to update your `.env` with the server and client hostnames. The default values for the dockerized frontend and backend look like this:

* `BASE_BACKEND_URL`: The default host for the dockerized container: `http://colmena_backend:5001`
* `BASE_FRONTEND_URL`: The default host for the frontend container: `http://colmena_frontend:5002`

The tests can be ran using docker by using the following commands:

Stops active containers, builds a new image and runs a new e2e container.

```bash
make docker.release
```

Containers that are already built can be re executed using:

```bash
make docker.rerun
```

You can peek or watch logs using the following commands:

```bash
make docker.logs
make docker.logs.watch
```

### Docker compose environment

The docker compose environment tries to reproduce a production-like environment and does not require any additional dependency. It uses images of the colmena backend and the colmena frontend instances. It starts a mailcrab and a postgres instance and finally executes the e2e tests run.

At the moment the compose execution requires to previously build the frontend and backend images. Use the `make docker.release` command for both the frontend and the backend repositories to generate new images.

> In the near future we expect to run backend and frontend healthly images from the gitlab registry. They're currently deployed in the CI/CD.

Make a copy of the *./devops/local/.env.example* and setup the environment variables:

```bash
cp ./devops/local/.env.example ./devops/local/.env
```

#### Envrionemnt

Most of the default values from the *.env.example* can be used.

**E2E variables:**

* `BASE_BACKEND_HOST`: The backend host. Default: `http://colmena_backend_e2e:5001`
* `BASE_FRONTEND_HOST`: The frontend host. Default: `http://colmena_frontend_e2e:5002`
* `EMAIL_HOST`: The mailing service hostname. Default: `colmena_mail_e2e:1080`
* `SUPERADMIN_EMAIL`: The superadmin email in a backend instance. Default: `admin@email`. **This value should be provided.**
* `SUPERADMIN_PASSWORD`: The superadmin password in a backend instance. Default: `somepassword`. **This value should be provided.**

**Postgres variables:**

* `POSTGRES_HOSTNAME`: The postgres hostname. Default: `postgres`
* `POSTGRES_USERNAME`: The postgres default username. Default: `postgres`
* `POSTGRES_PASSWORD`: The postgres password. Default: `postgres`

**Backend variables:**

* `DJANGO_SETTINGS_MODULE`: The settings module for the local development environment. Default: `colmena.settings.prod`
* `COLMENA_SECRET_KEY`: Secret key for the django project. Default: `some-secret-key`
* `POSTGRES_DATABASE`: A postgres database name for the e2e tests. Default: `colmena_e2e_prod`
* `STAGE`: The stage the backend should be run. Default: `local`
* `ALLOWED_HOSTS`: The allowed hosts to accept requests from. Default: `colmena_backend_e2e:5001 colmena_frontend_e2e:5002 localhost colmena_backend_e2e colmena_frontend_e2e`
* `CSRF_TRUSTED_ORIGINS`: Trusted origins. Default: `http://colmena_backend_e2e:5001 http://localhost:5001 http://0.0.0.0:5001`
* `CORS_ALLOWED_ORIGINS`: Allowed origins. Default: `http://colmena_frontend_e2e:5002`
* `PORT`: The backend the port should run. Default: `5001`

**Frontend variables:**

* `VITE_BACKEND_API_URL`: The server url. Defualt: `http://colmena_backend_e2e:5001`

The whole execution can be ran using:

```bash
make ENV_FILE=./devops/local/.env docker.e2e.start
```

The environment can be stopped or teared down using:

```bash
make docker.e2e.stop
```

or

```bash
make docker.e2e.down
```

## Structure of this project

* `fixtures`: Defines and create dummy data for tets such as: random usernames, emails, passwords, etc.
* `api`: Programatically interacts with external services.
* `selectors`: Scoped by pages that are visited, defines selectors for each element of a page such as: css selectors, xpath selectors or ids.
* `ws`: Interacts with the mailing service by watching emails reception.
